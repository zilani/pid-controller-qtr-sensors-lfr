// Use QTR sensor library for arduino.
// Link:https://github.com/pololu/qtr-sensors-arduino

#include <QTRSensors.h>

#define s1 A0
#define s2 A1
#define s3 A2
#define s4 A3
#define s5 A4
#define s6 A5
#define s7 A6
#define s8 A7
#define left A9
#define right A10
#define proximity 5 

#define leftMotor1   9
#define leftMotor2   8
#define rightMotor1  7
#define rightMotor2  4

#define leftEnable 2
#define rightEnable 3

#define NUM_SENSORS   8     // number of sensors used
#define TIMEOUT       2500  // waits for 2500 microseconds for sensor outputs to go low
#define EMITTER_PIN   A8     // emitter is controlled by digital pin 2
#define CALIBRATION_INDICATOR A15

char flw='b';

int error=0;
int p_error=0;

float Kp=42;
float Ki=0.6;
float Kd=220;

float drive=0;
float maxDrive=3500*(Kp-20) +5000 ;

int spd=220;

int ref_l=0;
int ref_r=0;


// sensors 0 through 7 are connected to digital pins 3 through 10, respectively
QTRSensorsRC qtrrc((unsigned char[]) {s1,s2,s3,s4,s5,s6,s7,s8},
  NUM_SENSORS, TIMEOUT, EMITTER_PIN); 

  
unsigned int sensorValues[NUM_SENSORS];
unsigned int pos=0;


void setup()
{
  pinMode (leftEnable, OUTPUT);
  pinMode (rightEnable, OUTPUT);
  
  pinMode (leftMotor1, OUTPUT);
  pinMode (leftMotor2, OUTPUT);
  pinMode (rightMotor1, OUTPUT);
  pinMode (rightMotor2, OUTPUT);
 /////////##################################///////////////////////// CALIBRATION//////
  pinMode(CALIBRATION_INDICATOR,OUTPUT);
  digitalWrite(CALIBRATION_INDICATOR,HIGH);
  
  int ir_max1=0;
  int ir_max2=0;
  int ir_min1=0;
  int ir_min2=0;
  int temp1=0;
  int temp2=0;
  
    for (int i = 0; i < 400; i++)  // make the calibration take about 10 seconds
  {
    qtrrc.calibrate();    // reads all sensors 10 times at 2500 us per read (i.e. ~25 ms per call)
    temp1=analogRead(left);
    temp2=analogRead(right);
   for(int i=0; i<20; i++){ 
    if(temp1>ir_max1) ir_max1=temp1;
    if(temp1<ir_min1) ir_min1=temp1;
    if(temp2>ir_max2) ir_max2=temp2;
    if(temp2<ir_min2) ir_min2=temp2;
   }
   
}
  
  ref_l=(ir_max1+ir_min1)/2;
  ref_r=(ir_max2+ir_min2)/2;
  
  digitalWrite(CALIBRATION_INDICATOR, LOW);     // turn off Arduino's LED to indicate we are through with calibration
 /////////##################################/////////////////////////
 
  ////////###################################///////////////////////////////
   // print the calibration minimum values measured when emitters were on
   Serial.begin(9600);
   Serial.print(ref_l);
   Serial.print(' ');
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(qtrrc.calibratedMinimumOn[i]);
    Serial.print(' ');
  }
  Serial.print(ref_r);
  Serial.println();
  
  // print the calibration maximum values measured when emitters were on
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(qtrrc.calibratedMaximumOn[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1000);


}




void loop()
{
  /*
  while(1){
    obstacle();
    r_motor_f(0);
    l_motor_f(0);
    delay(2000);
  }
  */
   if(digitalRead(proximity)==HIGH){
     obstacle();
   }
/*  
 while(1){
   /* qtrrc.read(sensorValues);
  for (int i = 0; i < NUM_SENSORS; i++){
    Serial.print(sensorValues[i]);
    Serial.print(' ');
  }
  
   Serial.print(analogRead(left));
   Serial.print(" ");
   Serial.print(analogRead(right));
    Serial.print(" ");
   Serial.print(ref_l);
    Serial.print(" ");
   Serial.print(ref_r);
   Serial.println();
  }
*/ 
   detect_line();
  
   if((analogRead(left) < ref_l) && (analogRead(right) >ref_r)){

  if(flw=='b'){
   while(!isOnLine()){  
       l_motor_f(150);
       r_motor_b(150);
   }
  }
   else if(flw=='w'){
     while(!isOnLine()){
      r_motor_f(150);
      l_motor_b(150);
     if(analogRead(right)<ref_l && analogRead(right)<ref_r){
        while(!isOnLine()){
         r_motor_b(100);
         l_motor_f(100);
        }
        break;
        }
    }
   }
 }
  else if((analogRead(left) > ref_l  ) && ( analogRead(right) <ref_r)){
    
       
     if(flw=='b'){
         while(!isOnLine()){
           r_motor_f(150);
           l_motor_b(150);
       }
       }
       
     else if(flw=='w'){
      while(!isOnLine()){
         l_motor_f(150);
         r_motor_b(150);
       if((analogRead(left)<ref_l) && (analogRead(right)<ref_r)){
         while(!isOnLine()){
             r_motor_f(100);
             l_motor_b(100);           
         }
        break;
        }
       }
     }
  }
       reads();
       follow_line();
      

 // show_up();  
  
  }





void detect_line(){
  
  qtrrc.read(sensorValues);
  if(sensorValues[0] <1500 && sensorValues[7] <1500){
      flw='b';
  
  }

  else if(sensorValues[0] >1500 && sensorValues[7] >1500){
      flw='w';
    }
//    Serial.println(flw);
  } 


void follow_line(){
  
    int lsp;
    int rsp;
    
    if(flw=='b'){          ///////////////////////// BLACK TRACK//////////////////
      
       if(drive>0){
       lsp=170-(drive*170)/80;
       rsp=170+(drive*80)/170;
       } 
       
       else  if(drive<0){
       lsp=170-(drive*80)/170;
       rsp=170+(drive*170)/80;
       } 
       
      if(lsp>250)lsp=250;
      if(rsp>250)rsp=250;
      if(lsp<0)lsp=0;
      if(rsp<0)rsp=0;
      
      if(abs(lsp-rsp)>230){
        if(lsp>rsp){
           l_motor_f(100);
           r_motor_b(100);
        }
        
        else if(rsp>lsp){
          l_motor_b(100);
          r_motor_f(100);
        }
      }
      else{
        l_motor_f(lsp);
        r_motor_f(rsp);
      }
  /*  
      Serial.print("          max :");
      Serial.print(maxDrive);     
      Serial.print("          Drive :");
      Serial.print(drive);
      Serial.print("          Error :");
      Serial.print(error);
      Serial.print("          Diff :");
      Serial.print(error-p_error);
      Serial.print("          Integ :");
      Serial.print(error+p_error);
      Serial.print("          pos :");
      Serial.print(pos);
      Serial.print("          lm_sp :");
      Serial.print(lsp);
      Serial.print("          rm_sp :");
      Serial.print(rsp);
      Serial.println();
      delay(200);   
*/
    }
     else if(flw=='w'){          ///////////////////////// WHITE TRACK//////////////////
       if(drive>0){
       lsp=170+(drive*170)/80;
       rsp=170-(drive*80)/170;
       } 
       
       else  if(drive<0){
       lsp=170+(drive*80)/170;
       rsp=170-(drive*170)/80;
       } 
       
      if(lsp>250)lsp=250;
      if(rsp>250)rsp=250;
      if(lsp<0)lsp=0;
      if(rsp<0)rsp=0;
      
      if(abs(lsp-rsp)>230){
        if(lsp>rsp){
           l_motor_f(100);
           r_motor_b(100);
        }
        
        else if(rsp>lsp){
          l_motor_b(100);
          r_motor_f(100);
        }
      }
      else{
        l_motor_f(lsp);
        r_motor_f(rsp);
      }
  /*  
      Serial.print("          max :");
      Serial.print(maxDrive);     
      Serial.print("          Drive :");
      Serial.print(drive);
      Serial.print("          Error :");
      Serial.print(error);
      Serial.print("          Diff :");
      Serial.print(error-p_error);
      Serial.print("          Integ :");
      Serial.print(error+p_error);
      Serial.print("          pos :");
      Serial.print(pos);
      Serial.print("          lm_sp :");
      Serial.print(lsp);
      Serial.print("          rm_sp :");
      Serial.print(rsp);
      Serial.println();
      delay(200);   
*/
    }
   
    p_error=error;

}

void reads(){
   
   pos = qtrrc.readLine(sensorValues);
   error=pos-3500;
   
   drive= ((Kp*error+Ki*(error+p_error)+ Kd*(error-p_error))/maxDrive)*170.0;
   
   if(drive>170) drive=170;
   else if(drive<-170) drive=-170;
   if(error<=-3450 || error>=3450) drive=0;   ////////////////////////////////////////////////////////////
 
 }


boolean isOnLine(){
   qtrrc.read(sensorValues);
  
  if(sensorValues[0] <1000 && sensorValues[1] <1000 && sensorValues[2] <1000 && sensorValues[3] <1000 && sensorValues[4] <1000 && sensorValues[5] <1000 && sensorValues[6] <1000 && sensorValues[7] <1000){
      return false;
  }
  else if(sensorValues[0] >2000 && sensorValues[1] >2000 && sensorValues[2] >2000 && sensorValues[3] >2000 && sensorValues[4] >2000 && sensorValues[5] >2000 && sensorValues[6] >2000 && sensorValues[7] >2000){
     return false;
  }
  else if((((sensorValues[0] >2000) && ( sensorValues[7] >2000)) && (sensorValues[3] <1000 || sensorValues[4] <1000))){
       return true;
 }
  else if((((sensorValues[0] <1000) && ( sensorValues[7] <1000)) && ( sensorValues[3] >2000 || sensorValues[4] >2000))){
          return true;
  }

  else return false;
   
}




void l_motor_f(int _speed)
{ analogWrite (leftEnable, _speed);
  digitalWrite (leftMotor1, HIGH);
  digitalWrite (leftMotor2, LOW);
}
void l_motor_b(int _speed)
{
  analogWrite (leftEnable, _speed);
  digitalWrite (leftMotor1, LOW);
  digitalWrite (leftMotor2, HIGH);
}

void r_motor_f(int _speed)
{ analogWrite (rightEnable, _speed);
  digitalWrite (rightMotor1, HIGH);
  digitalWrite (rightMotor2, LOW);
}
void r_motor_b(int _speed)
{
  analogWrite (rightEnable, _speed);
  digitalWrite (rightMotor1, LOW);
  digitalWrite (rightMotor2, HIGH);
}

void show_up(){
  l_motor_f(200);
  delay(500);
  l_motor_b(200);
  delay(500);
  l_motor_b(0);
  r_motor_f(200);
  delay(500);
  r_motor_b(200);
  delay(500);
  r_motor_b(0); 
}


void obstacle(){
  
  while(!isOnLine()){ 
    follow_line();
 //  delay(2);
  }
   r_motor_f(100);
   l_motor_f(0);
   
   delay(800);

int f_delay=800;

   r_motor_f(80);
   l_motor_f(80);
   
   delay(f_delay);
   
   l_motor_f(80);
   r_motor_f(0);
   delay(1000);
   
   l_motor_f(80);
   r_motor_f(80);
   delay(500);
   qtrrc.read(sensorValues);
   
   while((sensorValues[3]<1000 && sensorValues[4]<1000 && flw=='b')||(sensorValues[3]>2000 && sensorValues[4]>2000 && flw=='w')){
      l_motor_f(100);
      r_motor_f(60);
      delay(5);
      qtrrc.read(sensorValues);
   }
   
     l_motor_f(0);
     r_motor_f(100);  
    delay(400); 
  
   
}